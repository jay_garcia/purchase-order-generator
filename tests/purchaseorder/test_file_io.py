import logging
import random
import unittest
from lxml import etree as ET
from purchaseorder import PurchaseOrderXml, PurchaseOrderFile

log = logging.getLogger(__name__)


class TestSavePurchaseOrderXML(unittest.TestCase):

    def setUp(self):
        self.filename = 'test.xml'
        self.po_xml = PurchaseOrderXml()
        self.po_file = PurchaseOrderFile(self.filename, self.po_xml)
        self.invalid1 = 'notAnExEmEl.json'
        self.inv_file = PurchaseOrderFile(self.invalid1, self.po_xml)
        self.invalid2 = 'withA/InIt.xml'
        self.inv_file2 = PurchaseOrderFile(self.invalid2, self.po_xml)

    def test_save_template(self):
        assert str(self.po_file) == \
            f'/Users/portalink/vx_tools/assets/{self.filename}'
        assert not self.po_file.exists()  # Not yet saved
        result = self.po_file.save_file()  # Returns true if the save was OK
        assert result
        assert self.po_file.exists()  # File is now saved

    def test_save_invalid_name(self):
        with self.subTest('Non-XML extension'):
            assert not self.inv_file.exists()  # Not yet saved.
            result = self.inv_file.save_file()
            assert not result  # Not saved since ext is invalid
            assert not self.inv_file.exists()  # Still not saved!
        with self.subTest('Invalid file name'):
            assert not self.inv_file2.exists()
            result = self.inv_file2.save_file()
            assert not result  # Not saved since there's an offending char
            assert not self.inv_file2.exists()  # Also not saved!

    def test_edit_data(self):
        test_data = {
            'supID':  {
                'new_id': 'Test123'
            },
            'uniID': {
                'new_id': '321tseT'
            }
        }
        with self.subTest('Working test data edits'):
            assert self.po_file.edit_data(**test_data)
            identity_elems = self.po_xml._get_elems_by_tag('Identity')
            assert len(identity_elems) == 4
            assert identity_elems[0].text == '321tseT'
            assert identity_elems[1].text == 'Test123'

        with self.subTest('Order Number edit'):
            new_ord_no = f'TEST/000{random.randint(101,999)}'
            assert self.po_file.edit_data(ordNo={'order_number': new_ord_no})
            ordno_elem = self.po_xml._get_elems_by_tag('OrderRequestHeader')[0]
            assert ordno_elem.attrib['orderID'] == new_ord_no

        with self.subTest('Single-item edits'):
            new_ord_no = f'TEST/000{random.randint(101,999)}'
            assert self.po_file.edit_data(ordNo=new_ord_no)
            ordno_elem = self.po_xml._get_elems_by_tag('OrderRequestHeader')[0]
            assert ordno_elem.attrib['orderID'] == new_ord_no

        with self.subTest('Address-based edits'):
            new_address = {
                'isoCountryCode': 'AU',
                'addressID': '2 - SOME BRANCH',
                'name': 'SOME BRANCH',
                'DeliverTo': 'Some Branch, Not HO',
                'Street': '123 Elm Street',
                'City': 'Adelaide',
                'State': 'SA',
                'PostalCode': '5000',
                'Country': 'Australia',
                'Email': 'test@test.com',
            }
            fields = ['billAdd', 'shipAdd', ]
            params = {}
            for field in fields:
                with self.subTest(f'Testing the {field} field'):
                    params[field] = new_address
                    assert self.po_file.edit_data(**params)
                    params = {}
            _ = new_address.pop('addressID')

            params = {'conAdd': new_address}
            with self.subTest('Testing the conAdd field'):
                assert self.po_file.edit_data(**params)

        with self.subTest('Line item edits'):
            line_items = [
                {
                    "code": "ABCD123",
                    "desc": "Some Desc",
                    "qty": 1.000000,
                    "uom": "EA",
                    "price": 12.34,
                    "tax": 1.23,
                    "comments": "Test Comments",
                    "supplierID": "12345"
                }, {
                    "code": "123ABCD",
                    "desc": "Some Desc Too",
                    "qty": 2.000000,
                    "uom": "EA",
                    "price": 12.43,
                    "tax": 1.23,
                    "comments": "Test Comments",
                    "supplierID": "12345"
                },
            ]
            assert self.po_file.edit_data(lineItems=line_items)

    def test_invalid_edits(self):
        another_edit = {
            'new_id': 'ABC123',
            'old_id': '321tesT',
        }
        assert not self.po_file.edit_data(uniID=another_edit)

        _ = another_edit.pop('old_id')
        assert self.po_file.edit_data(uniID=another_edit)

        new_uni_elem = self.po_xml._get_elems_by_text('ABC123')
        assert new_uni_elem[0].tag == 'Identity'
        assert not self.po_file.edit_data(notavalidkey=another_edit)

        empty_lines = []
        assert not self.po_file.edit_data(lineItems=empty_lines)

        new_address = {'invalid': 'AU', }
        params = {}
        for key in ['billAdd', 'shipAdd', 'conAdd']:
            params[key] = new_address
            assert not self.po_file.edit_data(**params)
            params.clear()

    def test_save_with_edits(self):
        new_creds = {
            'user_id': 'TestPortalink',
            'password': 'NotARealPassword',
        }
        self.po_file.edit_data(creds=new_creds)
        assert self.po_file.save_file()
        parser = ET.XMLParser(ns_clean=True)
        edited_tree = ET.parse(
            f'/Users/portalink/vx_tools/assets/{self.filename}',
            parser=parser
        )
        creds_user_field = edited_tree.xpath('//*[text()="TestPortalink"]')
        assert len(creds_user_field) == 1
        assert creds_user_field[0].tag == 'Identity'
        creds_pass_field = edited_tree.xpath('//SharedSecret')
        assert len(creds_pass_field) == 1
        assert creds_pass_field[0].text == 'NotARealPassword'

    def test_print_data(self):
        print_data = ET.tostring(self.po_xml.tree)
        assert print_data == self.po_file.print_data()
        print_data2 = ET.tostring(self.po_xml.tree, pretty_print=True)
        assert print_data2 == self.po_file.print_data(pretty_print=True)

    def tearDown(self):
        self.po_file.unlink()


if __name__ == '__main__':
    unittest.main()
