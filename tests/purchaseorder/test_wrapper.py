import logging
import pytest
import unittest
from decimal import Decimal
from purchaseorder import PurchaseOrderXml
from typing import Callable


log = logging.getLogger(__name__)


def change_address_positive(tc: object,
                            method: Callable[[dict, ], bool],
                            **kwargs):
    """DRY for testing the address section

    Arguments:
        tc {object} -- the Test Case object (i.e., self)

        method {Callable} -- the function to be tested

        kwargs {dict} -- contains:

            new_address -- the data dict to be used for testing.

            xpath_head -- the branch head XPATH to be used for testing.

    """
    result = method(kwargs['new_address'])
    xpath_head = kwargs['xpath_head']
    assert result
    for key, value in kwargs['new_address'].items():
        with tc.subTest(f'Testing the {key} field'):
            if key[0].isupper():
                xpath = f'{xpath_head}/descendant::{key}'
                log.info(f'Checking in this xpath: {xpath}')
                elem = tc.po_xml._get_elems_by_xpath(xpath)
                assert elem[0].tag == key
                assert elem[0].text == value
            else:
                xpath = f'{xpath_head}/descendant::*[@{key}]'
                log.info(f'Checking in {xpath}...')
                elems = tc.po_xml._get_elems_by_xpath(xpath)
                for elem in elems:
                    with tc.subTest(f'Attribute change on {elem.tag}'):
                        log.info(elem.tag)
                        log.info(elem.attrib[key])
                        assert elem.attrib[key] == value


def change_address_negative(tc: object,
                            method: Callable[[dict, ], bool],
                            **kwargs):
    """DRY for testing the address section

    Arguments:
        tc {object} -- the Test Case object (i.e., self)

        method {Callable} -- the function to be tested

        kwargs {dict} -- contains:

            new_address -- the data dict to be used for testing.

            xpath_head -- the branch head XPATH to be used for testing.

    """
    result = method(kwargs['new_address'])
    xpath_head = kwargs['xpath_head']
    assert not result
    for key, value in kwargs['new_address'].items():
        with tc.subTest(f'Testing the {key} field'):
            if key[0].isupper():
                xpath = f'{xpath_head}/descendant::{key}'
                log.info(f'Checking in this xpath: {xpath}')
                with pytest.raises(LookupError) as excinfo:
                    tc.po_xml._get_elems_by_xpath(xpath)
                assert 'No elements found' in str(excinfo.value), \
                    'Elements were found in the template.'
            else:
                xpath = f'{xpath_head}/descendant::*[@{key}]'
                log.info(f'Checking in {xpath}...')
                with pytest.raises(LookupError) as excinfo:
                    tc.po_xml._get_elems_by_xpath(xpath)
                assert 'No elements found' in str(excinfo.value), \
                    'Elements were found in the template.'


class TestTemplatePurchaseOrderXML(unittest.TestCase):

    def setUp(self):
        self.po_xml = PurchaseOrderXml()

    def test_read_xml_template(self):
        # Confirm that the file read is the PO XML template.
        assert self.po_xml.template == \
            'po_template.xml', \
            'XML read is not the template.'

        supplier_id_elems = self.po_xml._get_elems_by_text('751428732')
        for elem in supplier_id_elems:
            with self.subTest(f'Supplier ID is located in the correct tag:\
                              {elem.tag}'):
                assert elem.tag in \
                    ['SupplierID', 'Identity'], \
                    'ID found in a different tag'

        uni_id_elems = self.po_xml._get_elems_by_text('743574191')
        for elem in uni_id_elems:
            with self.subTest(f'Uni ID is located in the correct tag:\
                              {elem.tag}'):
                assert elem.tag == \
                    'Identity', \
                    'ID found in a different tag'

    def test_change_supplier_text(self):
        new_id = '123456'
        # New function that changes the Supplier ID.
        result = self.po_xml.change_supplier_id(new_id)
        assert result
        for elem in self.po_xml._get_elems_by_text(new_id):
            with self.subTest(f'Supplier ID changed from old to {new_id}\
                              in {elem.tag}'):
                assert elem.text == \
                    new_id, \
                    'ID not changed on submitting a new ID for it.'

        old_id = 'test123'
        result = self.po_xml.change_supplier_id(old_id=old_id, new_id=new_id)
        assert not result
        with pytest.raises(LookupError) as excinfo:
            self.po_xml._get_elems_by_text(old_id)
        assert str(excinfo.value) == 'No elements found.'

    def test_change_university_text(self):
        new_id = '123456'
        # New function that changes the Supplier ID.
        result = self.po_xml.change_university_id(new_id)
        assert result
        for elem in self.po_xml._get_elems_by_text(new_id):
            with self.subTest(f'Supplier ID changed from old to {new_id}\
                              in {elem.tag}'):
                assert elem.text == \
                    new_id, \
                    'ID not changed on submitting a new ID for it.'

        old_id = 'test123'
        result = self.po_xml.change_university_id(old_id=old_id, new_id=new_id)
        assert not result
        with pytest.raises(LookupError) as excinfo:
            self.po_xml._get_elems_by_text(old_id)
        assert str(excinfo.value) == 'No elements found.'

    def test_change_sender_creds_text(self):
        new_user_id = 'test_uni'
        new_user_pass = 'test123'
        result = self.po_xml.change_sender_credentials(
            new_user_id, new_user_pass
        )
        assert result
        user_id_elem = self.po_xml.tree.xpath(f'//*[text()="{new_user_id}"]')
        assert user_id_elem[0].tag == 'Identity'
        user_pass_elem = self.po_xml.tree.xpath(
            f'//*[text()="{new_user_pass}"]')
        assert user_pass_elem[0].tag == 'SharedSecret'

        old_user_id = 'not_the_right_one'
        assert not self.po_xml.change_sender_credentials(
            new_user_id,
            new_user_pass,
            old_user_id=old_user_id
        )

        old_password = 'also_not_the_right_one'
        assert not self.po_xml.change_sender_credentials(
            new_user_id,
            new_user_pass,
            old_password=old_password
        )

    def test_change_order_number(self):
        new_order_number = 'MAIN000/1234-TEST'
        assert self.po_xml.change_order_number(new_order_number)
        ord_header = self.po_xml._get_elems_by_xpath(
            f'//*[@orderID="{new_order_number}"]'
        )[0]
        assert ord_header.tag == 'OrderRequestHeader'

    def test_change_billto_address(self):
        xpath_head = '//BillTo'
        new_bill_to = {
            'isoCountryCode': 'AU',
            'addressID': '2 - SOME BRANCH',
            'name': 'SOME BRANCH',
            'DeliverTo': 'Some Branch, Not HO',
            'Street': '123 Elm Street',
            'City': 'Adelaide',
            'State': 'SA',
            'PostalCode': '5000',
            'Country': 'Australia',
            'Email': 'test@test.com',
        }
        change_address_positive(self, self.po_xml.change_bill_address,
                                xpath_head=xpath_head, new_address=new_bill_to)

    def test_incorrect_billto_format(self):
        xpath_head = '//BillTo'
        new_bill_to = {'NotAValidBillTag': 'AU', 'notAValidAttrib': 'One'}
        change_address_negative(self, self.po_xml.change_bill_address,
                                xpath_head=xpath_head, new_address=new_bill_to)

    def test_change_shipto_address(self):
        xpath_head = '//ShipTo'
        new_ship_to = {
            'isoCountryCode': 'AU',
            'addressID': '2 - SOME BRANCH',
            'name': 'SOME BRANCH',
            'DeliverTo': 'Some Branch, Not HO',
            'Street': '123 Elm Street',
            'City': 'Adelaide',
            'State': 'SA',
            'PostalCode': '5000',
            'Country': 'Australia',
            'Email': 'test@test.com',
        }
        change_address_positive(self, self.po_xml.change_ship_address,
                                xpath_head=xpath_head, new_address=new_ship_to)

    def test_incorrect_shipto_format(self):
        xpath_head = '//ShipTo'
        new_ship_to = {'NotAValidBillTag': 'AU', 'notAValidAttrib': 'One'}
        change_address_negative(self, self.po_xml.change_ship_address,
                                xpath_head=xpath_head, new_address=new_ship_to)

    def test_change_contact_address(self):
        xpath_head = '//Contact'
        new_contact = {
            'isoCountryCode': 'AU',
            'name': 'SOME BRANCH',
            'DeliverTo': 'Some Branch, Not HO',
            'Street': '123 Elm Street',
            'City': 'Adelaide',
            'State': 'SA',
            'PostalCode': '5000',
            'Country': 'Australia',
            'Email': 'test@test.com',
        }
        change_address_positive(self, self.po_xml.change_contact_address,
                                xpath_head=xpath_head, new_address=new_contact)

    def test_incorrect_contact_format(self):
        xpath_head = '//Contact'
        new_contact = {'NotAValidBillTag': 'AU', 'notAValidAttrib': 'One'}
        change_address_negative(self, self.po_xml.change_bill_address,
                                xpath_head=xpath_head, new_address=new_contact)

    def test_change_line_items(self):
        """
        Given a list of dict of items, change the XML to get
        these line items generated.
        """
        sample_line_items = [
            {
                "code": "ABCD123",
                "desc": "Some Desc",
                "qty": 1.000000,
                "uom": "EA",
                "price": 12.34,
                "tax": 1.23,
                "comments": "Test Comments",
                "supplierID": "12345"
            }, {
                "code": "123ABCD",
                "desc": "Some Desc Too",
                "qty": 2.000000,
                "uom": "EA",
                "price": 12.43,
                "tax": 1.23,
                "comments": "Test Comments",
                "supplierID": "12345"
            },
        ]
        with self.subTest('Line Items get updated'):
            result = self.po_xml.change_line_items(sample_line_items)
            assert result

            xpath_head = '//ItemOut'
            for x, item in enumerate(sample_line_items):
                path = f'{xpath_head}[@lineNumber={x+1}]'

                code_elem = self.po_xml._get_elems_by_xpath(
                    f'{path}/descendant::SupplierPartID'
                )[0]
                assert code_elem.text == item['code']

                desc_elem = self.po_xml._get_elems_by_xpath(
                    f'{path}/descendant::Description'
                )[0]
                assert desc_elem.text == item['desc']

                qty_elem = self.po_xml._get_elems_by_xpath(path)[0]
                assert qty_elem.attrib['quantity'] == str(item['qty'])

                uom_elem = self.po_xml._get_elems_by_xpath(
                    f'{path}/descendant::UnitOfMeasure'
                )[0]
                assert uom_elem.text == item['uom']

                price_elem = self.po_xml._get_elems_by_xpath(
                    f'{path}/descendant::UnitPrice/Money'
                )[0]
                assert price_elem.text == str(item['price'])

                tax_elem = self.po_xml._get_elems_by_xpath(
                    f'{path}/descendant::Tax/Money'
                )[0]
                assert tax_elem.text == str(item['tax'])

        with self.subTest('Line Totals and Tax get updated'):
            total_elem = self.po_xml._get_elems_by_xpath(
                '//OrderRequestHeader/Total/Money'
            )[0]
            tax_elem = self.po_xml._get_elems_by_xpath(
                '//OrderRequestHeader/Tax/Money'
            )[0]

            assert Decimal(total_elem.text) == Decimal('37.20')
            assert Decimal(tax_elem.text) == Decimal('3.72')

    def test_invalid_change_line_items(self):
        sample_line_items = []  # Empty!
        result = self.po_xml.change_line_items(sample_line_items)
        assert not result

        sample_line_items.append({'notAProperCode': 'blah blah'})
        result = self.po_xml.change_line_items(sample_line_items)
        assert not result


class TestCustomPurchaseOrderXML(unittest.TestCase):

    def setUp(self):
        self.template_name = 'po_template_other.xml'
        self.po_xml = PurchaseOrderXml(self.template_name)
        self.old_uni_id = 'Some Other ID'
        self.old_sup_id = 'Also Other ID'

    def test_read_xml_template(self):
        # Confirm that the file read is the PO XML template.
        assert self.po_xml.template == \
            self.template_name, \
            'XML read is not the template.'

        with pytest.raises(LookupError) as sup_found:
            self.po_xml._get_elems_by_text('751428732')
        assert 'No elements found' in str(sup_found.value), \
            'Elements were found in the template.'

        with pytest.raises(LookupError) as uni_found:
            self.po_xml._get_elems_by_text('743574191')
        assert 'No elements found' in str(uni_found.value), \
            'Elements were found in the template.'

    def test_change_supplier_id(self):
        new_id = 'Test123'
        supplier_id_changed = self.po_xml.change_supplier_id(
            old_id=self.old_sup_id, new_id=new_id)
        assert supplier_id_changed

        for elem in self.po_xml._get_elems_by_text(new_id):
            with self.subTest(f'Supplier ID changed from {self.old_sup_id} \
                                to {new_id} in {elem.tag}'):
                assert elem.text == new_id, 'Text did not change.'

    def test_change_university_id(self):
        new_id = '321tseT'
        uni_id_changed = self.po_xml.change_university_id(
            old_id=self.old_uni_id, new_id=new_id)
        assert uni_id_changed

        for elem in self.po_xml._get_elems_by_text(new_id):
            with self.subTest(f'University ID changed from {self.old_uni_id} \
                                to {new_id} in {elem.tag}'):
                assert elem.text == new_id, 'Text did not change.'

    def test_change_billto_address(self):
        xpath_head = '//BillTo'
        new_bill_to = {
            'isoCountryCode': 'AU',
            'addressID': '2 - SOME BRANCH',
            'name': 'SOME BRANCH',
            'DeliverTo': 'Some Branch, Not HO',
            'Street': '123 Elm Street',
            'City': 'Adelaide',
            'State': 'SA',
            'PostalCode': '5000',
            'Country': 'Australia',
            'Email': 'test@test.com',
        }
        change_address_positive(self, self.po_xml.change_bill_address,
                                xpath_head=xpath_head, new_address=new_bill_to)

    def test_change_shipto_address(self):
        xpath_head = '//ShipTo'
        new_ship_to = {
            'isoCountryCode': 'AU',
            'addressID': '2 - SOME BRANCH',
            'name': 'SOME BRANCH',
            'DeliverTo': 'Some Branch, Not HO',
            'Street': '123 Elm Street',
            'City': 'Adelaide',
            'State': 'SA',
            'PostalCode': '5000',
            'Country': 'Australia',
            'Email': 'test@test.com',
        }
        change_address_positive(self, self.po_xml.change_ship_address,
                                xpath_head=xpath_head, new_address=new_ship_to)

    def test_change_contact_address(self):
        xpath_head = '//Contact'
        new_contact = {
            'isoCountryCode': 'AU',
            'name': 'SOME BRANCH',
            'DeliverTo': 'Some Branch, Not HO',
            'Street': '123 Elm Street',
            'City': 'Adelaide',
            'State': 'SA',
            'PostalCode': '5000',
            'Country': 'Australia',
            'Email': 'test@test.com',
        }
        change_address_positive(self, self.po_xml.change_contact_address,
                                xpath_head=xpath_head, new_address=new_contact)


if __name__ == '__main__':
    unittest.main()
