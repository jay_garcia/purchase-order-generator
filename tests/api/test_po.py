import json
import logging
import unittest
import random
from purchaseorder import PurchaseOrderFile, PurchaseOrderXml
from api import PurchaseOrderSender

log = logging.getLogger(__name__)
LINE_ITEMS_PATH = '/Users/portalink/vx_tools/assets/lineitems.json'


class TestSendPurchaseOrderXML(unittest.TestCase):

    def setUp(self):
        self.filename1 = 'test1.xml'
        self.filename2 = 'test2.xml'
        self.po_xml = PurchaseOrderXml()
        self.po_file1 = PurchaseOrderFile(self.filename1, self.po_xml)
        self.po_file2 = PurchaseOrderFile(self.filename2, self.po_xml)
        self.po_sender1 = PurchaseOrderSender(
            self.po_file1, uni_id='gv48GGrQqC1vyNSliS64z')
        self.po_sender2 = PurchaseOrderSender(self.po_file2)
        with open(LINE_ITEMS_PATH, 'rb') as lines:
            self.line_items_source = json.loads(lines.read())
        for x in self.line_items_source:
            x.update({'supplierID': '751428731'})

    def test_post_xml_to_api(self):
        self.po_file1.edit_data(ordNo=f'TST/000{random.randint(101, 999)}')
        random_lines = random.choices(
            self.line_items_source, k=random.randint(1, 20))
        self.po_file1.edit_data(lineItems=random_lines)
        result = self.po_sender1.post_xml()
        assert result
        invalid_data = {
            'supID':  {
                'new_id': 'Test123'
            },
            'uniID': {
                'new_id': 'gv48GGrQqC1vyNSliS64z'
            }
        }
        self.po_file2.edit_data(**invalid_data)
        result = self.po_sender2.post_xml()
        assert not result  # Supplier ID not configured

    def tearDown(self):
        self.po_file1.unlink()
        self.po_file2.unlink()
