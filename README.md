# VX TOOLS - Purchase Order Generator

[![made-with-python](https://img.shields.io/badge/Made%20with-Python-1f425f.svg)](https://www.python.org/) [![codecov](https://codecov.io/bb/plintegrationteammanila/purchase-order-generator/branch/master/graph/badge.svg?token=iQFFsTUVaf)](https://codecov.io/bb/plintegrationteammanila/purchase-order-generator)

## Rationale

These helper classes allow the Portalink team to test the APIs available for the Universities' use. It allows the following functions:

* Generate a sample Purchase Order XML
* Edit the fields in the XML:
    * Supplier ID
    * University ID
    * Credentials
    * Addresses (Billing, Shipping, Contact)
    * Line Items (randomly-generated from a dump of line items)
* Send the PO XML via API based on given parameters:
    * University ID
    * Supplier ID
    * Authentication (On/Off)
