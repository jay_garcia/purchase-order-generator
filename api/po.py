import json
import logging
import requests
from purchaseorder import PurchaseOrderFile

log = logging.getLogger(__name__)

WORKER_API = "https://worker-api.{}.vendor.exchange"
DEFAULT_HEADERS = {'Content-Type': 'text/xml', }
AUTH = ('test_uni2', 'Qs756MjrCDWG')
UNI_ID = '8l2ZLM3NYcJ9ofhnxTtY8'
STAGE = 'staging'


class PurchaseOrderSender:
    """
    Class wrapper for the Purchase Order API calls
    """

    def __init__(self,
                 file: PurchaseOrderFile,
                 endpoint=WORKER_API,
                 stage=STAGE,
                 uni_id=UNI_ID,
                 auth=False):
        self.file = file
        self.uni_id = uni_id
        self.endpoint = endpoint.format(stage)
        self.po_url = f'{self.endpoint}/ext/org/{uni_id}/order'
        self.auth = auth

    def post_xml(self) -> bool:
        self.file.edit_data(
            creds={
                'user_id': AUTH[0],
                'password': AUTH[1],
            }
        )
        if not self.file.exists():
            self.file.save_file()
        try:
            if self.auth:
                response = requests.post(
                    self.po_url,
                    data=self.file.print_data(),
                    headers=DEFAULT_HEADERS,
                    auth=AUTH
                )
            else:
                response = requests.post(
                    self.po_url,
                    data=self.file.print_data(),
                    headers=DEFAULT_HEADERS
                )

            if response.status_code != 201:
                raise ValueError(f'Received a {response.status_code}.')
                log.error(json.loads(response.content))
            return True
        except ValueError as ie:
            log.error(ie)
            return False
