import logging
from decimal import Decimal, getcontext
from lxml import etree as ET
from pathlib import Path

ASSETS_FOLDER = '/Users/portalink/vx_tools/assets'

log = logging.getLogger(__name__)


class PurchaseOrderXml:
    '''
    Stub implementation of the Purchase Order XML reader
    '''

    def __init__(self,
                 template='po_template.xml',
                 supplier_id='751428732',
                 university_id='743574191',
                 order_number='MAIN/000108',
                 precision=6):
        self.template = template
        self.supplier_id = supplier_id
        self.university_id = university_id
        self.order_number = order_number
        self.parser = ET.XMLParser(ns_clean=True)
        self.template_path = Path(f'{ASSETS_FOLDER}/{template}')
        self.tree = ET.parse(str(self.template_path), parser=self.parser)
        getcontext().prec = precision

    def _get_elems_by_text(self, id: str) -> list:
        if self.tree.xpath(f'//*[text()="{id}"]'):
            return self.tree.xpath(f'//*[text()="{id}"]')
        else:
            raise LookupError('No elements found.')

    def _get_elems_by_tag(self, tag: str) -> list:
        return self.tree.xpath(f'//{tag}')

    def _get_elems_by_xpath(self, xpath: str) -> list:
        if self.tree.xpath(xpath):
            return self.tree.xpath(xpath)
        else:
            raise LookupError('No elements found.')

    def _change_address_section(self, branch_xpath: str, data: dict) -> bool:
        """
        Changes all fields with a given dict in a given XPath "branch"

        Arguments:

            xpath {str} -- The XPath where the branch is located.

            data {dict} -- The set of fields to be edited.
            The convention is
                key = Tag or attrib to be edited;
                value = replacement value.
            i.e.,
                {'foo': 'bar'} replaces all text in tags with an _attribute_
                               *foo* with 'bar'.
                {'Foo': 'bar'} replaces all text in _tags_ with 'bar'.

        Returns:
            bool -- True if successful, False if otherwise.
        """
        try:
            for key, value in data.items():
                if key[0].isupper():
                    xpath = f'{branch_xpath}/descendant::{key}'
                    log.info(xpath)
                    element = self._get_elems_by_xpath(xpath)
                    element[0].text = value
                else:
                    xpath = f'{branch_xpath}/descendant::*[@{key}]'
                    log.info(xpath)
                    elems = self._get_elems_by_xpath(xpath)
                    for elem in elems:
                        elem.attrib[key] = value
            return True
        except LookupError as e:
            log.error(e)
            return False

    def change_supplier_id(self, new_id, old_id=None):
        old_id = self.supplier_id if not old_id else old_id
        try:
            for elem in self._get_elems_by_text(old_id):
                elem.text = new_id
            return True
        except LookupError as ie:
            log.error(ie)
            return False

    def change_university_id(self, new_id, old_id=None):
        old_id = self.university_id if not old_id else old_id
        try:
            for elem in self._get_elems_by_text(old_id):
                elem.text = new_id
            return True
        except LookupError as ie:
            log.error(ie)
            return False

    def change_sender_credentials(self, user_id, password,
                                  old_user_id='portalink_testuni',
                                  old_password='SharedSecret'):
        try:
            user_elem = self._get_elems_by_text(old_user_id)
            user_elem[0].text = user_id
            pass_elem = self._get_elems_by_tag(old_password)
            pass_elem[0].text = password
            return True
        except LookupError as ie:
            log.info(ie)
            return False

    def change_order_number(self, order_number, old_no=None) -> bool:
        old_no = self.order_number if not old_no else old_no
        ord_header = self._get_elems_by_tag('OrderRequestHeader')[0]
        ord_header.attrib['orderID'] = order_number
        return True

    def change_bill_address(self, bill_address: dict) -> bool:
        bpath = '//BillTo'
        return self._change_address_section(bpath, bill_address)

    def change_ship_address(self, ship_address: dict) -> bool:
        spath = '//ShipTo'
        return self._change_address_section(spath, ship_address)

    def change_contact_address(self, contact_address: dict) -> bool:
        cpath = '//Contact'
        return self._change_address_section(cpath, contact_address)

    def change_line_items(self, line_items: list) -> bool:
        try:
            # Step 1: Clear out all existing line items.
            if not line_items:
                raise LookupError('Empty line items.')
            for x in self._get_elems_by_xpath('//ItemOut'):
                x.getparent().remove(x)
            log.info(ET.tostring(self.tree, pretty_print=True))

            new_lines = []
            totals = []

            # Step 2: For each line item, create a new ItemOut
            for c, item in enumerate(line_items):
                totals.append(Decimal(item['qty']) * Decimal(item['price']))
                item_attr = {
                    'quantity': str(item['qty']),
                    'lineNumber': str(c + 1),
                    'requestedDeliveryDate': '2019-09-17T00:00:00+10:00'
                }
                new_item = ET.Element('ItemOut', item_attr)

                code_section = ET.SubElement(new_item, 'ItemID')
                product_code = ET.SubElement(code_section, 'SupplierPartID')
                product_code.text = item['code']  # Code field

                detail_section = ET.SubElement(new_item, 'ItemDetail')
                unit_price = ET.SubElement(detail_section, 'UnitPrice')
                price = ET.SubElement(unit_price, 'Money')
                price.attrib['currency'] = 'AUD'
                price.text = str(item['price'])  # Price field
                description = ET.SubElement(detail_section, 'Description')
                # Lang fix: https://bendustries.org/wp/?p=21
                attr = description.attrib
                attr['{http://www.w3.org/XML/1998/namespace}lang'] = 'en'
                description.text = item['desc']  # Description field
                uom = ET.SubElement(detail_section, 'UnitOfMeasure')
                uom.text = item['uom']  # UOM field
                classification = ET.SubElement(detail_section,
                                               'Classification')
                classification.attrib['domain'] = ''

                supplier_section = ET.SubElement(new_item,
                                                 'SupplierID',
                                                 {'domain': 'DUNS'})
                supplier_section.text = item['supplierID']

                tax_section = ET.SubElement(new_item, 'Tax')
                tax = ET.SubElement(tax_section,
                                    'Money', {'currency': 'AUD'})
                tax.text = str(item['tax'])
                tax_desc = ET.SubElement(tax_section, 'Description')
                attr = tax_desc.attrib
                attr['{http://www.w3.org/XML/1998/namespace}lang'] = 'en'
                tax_desc.text = 'GST'

                comment_section = ET.SubElement(new_item, 'Comments')
                attr = comment_section.attrib
                attr['{http://www.w3.org/XML/1998/namespace}lang'] = 'en'
                comment_section.text = item.get('comments', None)

                log.info(ET.tostring(new_item, pretty_print=True))
                new_lines.append(new_item)

            # Step 3: Attach these lines back into the XML
            or_branch = self._get_elems_by_tag('OrderRequest')
            for item in new_lines:
                or_branch[0].append(item)

            # Step 4: Update the totals
            total_elem = self._get_elems_by_xpath(
                '//OrderRequestHeader/Total/Money')[0]
            total_elem.text = str(sum(totals))
            tax_elem = self._get_elems_by_xpath(
                '//OrderRequestHeader/Tax/Money')[0]
            tax_elem.text = str(sum(totals) * Decimal(0.1))
            return True
        except (LookupError, KeyError) as ie:
            log.error(ie)
            return False


if __name__ == '__main__':
    new_po_xml = PurchaseOrderXml()
    line_items = [
        {
            'code': 'ABCD123',
            'desc': 'Some Desc',
            'qty': 1.000000,
            'uom': 'EA',
            'price': 12.34,
            'tax': 1.23,
            'supplierID': '12345'
        },
    ]  # Empty!
    new_po_xml.change_line_items(line_items)
