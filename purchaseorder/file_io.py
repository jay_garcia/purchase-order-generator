import logging
from lxml import etree as ET  # noqa: F401
from pathlib import Path
from . import PurchaseOrderXml

ASSETS_FOLDER = '/Users/portalink/vx_tools/assets'

log = logging.getLogger(__name__)


class PurchaseOrderFile:
    """
    Class for saving the Purchase Order and
    transmitting it over API
    """

    def __init__(self,
                 file,
                 po_xml: PurchaseOrderXml,):
        self.po_xml = po_xml
        self.file = Path(f'{ASSETS_FOLDER}/{file}')

    def __str__(self):
        return str(self.file)

    def exists(self) -> bool:
        return self.file.exists()

    def unlink(self):
        try:
            self.file.unlink()
        except FileNotFoundError:
            pass

    def save_file(self) -> bool:
        try:
            if self.file.suffix != '.xml':
                raise ValueError('Must be of extension .xml')
            with open(str(self), 'xb') as file:
                self.po_xml.tree.write(
                    file,
                    encoding='UTF-8',
                    pretty_print=True
                )
            return True
        except FileNotFoundError as e:
            log.error(e)
            log.info('Your file name seems invalid.\
                Strip out any invalid chars to continue.')
            return False
        except ValueError as e:
            log.error(e)
            return False

    def edit_data(self, **kwargs: dict) -> bool:
        func_map = {
            'supID': self.po_xml.change_supplier_id,
            'uniID': self.po_xml.change_university_id,
            'creds': self.po_xml.change_sender_credentials,
            'billAdd': self.po_xml.change_bill_address,
            'shipAdd': self.po_xml.change_ship_address,
            'conAdd': self.po_xml.change_contact_address,
            'lineItems': self.po_xml.change_line_items,
            'ordNo': self.po_xml.change_order_number,
        }
        try:
            dict_only = ['billAdd', 'shipAdd', 'conAdd', ]
            for key, value in kwargs.items():
                # run the function for that key
                if key not in func_map.keys():
                    raise LookupError(f'Error in {key}, not a valid function')
                if type(value) == dict and \
                        key not in dict_only and \
                        not func_map[key](**value):
                    raise LookupError(
                        'Something went wrong, check your variables.')
                elif key in dict_only and \
                        not func_map[key](value):
                    raise LookupError(
                        'Something went wrong, check your variables.')
                elif type(value) in [str, list] and \
                        not func_map[key](value):
                    raise LookupError(
                        'Somethin went wrong, check your variable')
            return True
        except LookupError as ie:
            log.error(ie)
            return False

    def print_data(self, **kwargs):
        return ET.tostring(self.po_xml.tree, **kwargs)


if __name__ == '__main__':
    new_po_xml = PurchaseOrderXml()
    new_po_io = PurchaseOrderFile('test.xml', new_po_xml)
    new_po_io.save_file()
