# %%
import json
import random
from purchaseorder import PurchaseOrderFile, PurchaseOrderXml
from api import PurchaseOrderSender

# %%
lineitems = 'assets/lineitems.json'
with open(lineitems, 'rb') as file:
    lines_list = json.load(file)

for x in lines_list:
    x.update({'supplierID': '751428731'})

# %%
for x in range(1, 100):
    ordno = f'RD3/000{random.randint(101, 999)}'
    clean_ordno = ordno.replace('/', '')
    po_xml = PurchaseOrderXml()
    po_file = PurchaseOrderFile(f'test{clean_ordno}.xml', po_xml=po_xml)
    po_sender = PurchaseOrderSender(po_file, uni_id='gv48GGrQqC1vyNSliS64z')
    numlines = random.randint(2, 20)
    lines = random.choices(lines_list, k=numlines)
    po_file.edit_data(
        ordNo={'order_number': ordno},
        lineItems={'line_items': lines})
    po_file.save_file()
    if po_sender.post_xml():
        print(f'Order Number {ordno} successful')
    else:
        print(f'OrderNumber {ordno} failed in sending')
    if po_file.exists():
        po_file.unlink()


# %%
