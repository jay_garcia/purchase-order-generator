# Notes

## TODOS

1. Create test cases for:
    * ~~Ability to read an XML file.~~ DONE
    * ~~Ability to change specific fields in the XML.~~ DONE
    * ~~Ability to save the XML file with a given name/reference.~~ DONE
2. Create quick stories:
    * Purpose
    * Things to do
3. New Story:
    * ~~Transmit the XML over via API.~~ DONE
    * Capture the response from it.
    * Submit multiple orders as needed.

## STORIES

As a user of this app, I want to be able to:

### STORY 1: Generate a Purchase Order XML based on a given set of parameters

#### Acceptance Criteria

1. Read the existing XML as the template. DONE
2. Modify the XML based on the parameters below. DONE
3. Specify to which Supplier the Purchase Order gets submitted. DONE
4. Specify from which University the Purchase Order is coming from. DONE
5. Specify the line items I can use in the test Purchase Order. DONE
   * items codes
   * prices
   * units of measure
   * special comments
6. Specify the Sender's credentials in the XML. DONE
7. Specify the Delivery Details of the Purchase Order. DONE
   * Bill To Address
   * Ship To Address
   * Contact Person
8. Save the changes applied to the Purchase Order XML with a given name/location. DONE
9. Specify the Order Reference Number for it. DONE
10. Calculate the totals based on the line items. DONE

### STORY 2: Transmit a generated Purchase Order to the API

#### Acceptance Criteria

1. Define the number of XMLs to be sent. WIP
2. Define the Order Reference Number format. WIP
3. Define the number of line items to be added (random, the same, etc.). WIP
4. Define the API parameters to be used in the transmission. WIP
5. Specify the number of retries to be done per API call.

## CODE SMELL

Things that can be refactored or improved:

1. If a single item is submitted in `edit_data` per variable, treat the submitted data as a positional arg. WIP
2. If a dict was passed instead, then submit it as `**values` to the function.
3. When initializing a new `PurchaseOrderXML`, the attributes sent to the `__init__` function can be edited straight away.
4. When running functions that change a value in the XML, persist the change as part of the object.
