'''
1) Generate a new set of POs based on:
   * Supplier/DUNS ID
   * Number of orders to be generated
   * Line items
2) Send the generated XML over to the API once complete.
3) Print the generated order numbers.

'''

#%%
from lxml import etree as ET
from pathlib import Path

#%%
filename = '~/Downloads/Vendor Exchange/unisaTestOrder.xml'
xml_path = Path(filename)
parser = ET.XMLParser(ns_clean=True)

#%%
with open(xml_path.expanduser(), 'r') as file:
    tree = ET.parse(file)
    # print(ET.tostring(tree))

#%%
# Find all test that has the current ID
r_all_with_id = tree.xpath('//*[text()="751428732"]')
# Change it for all the times it appeared.
for x in r_all_with_id:
    x.text = '751428731'

#%%
# Save the file with a new name
tree.write('test.xml', encoding='UTF-8', pretty_print=True, xml_declaration=True)


